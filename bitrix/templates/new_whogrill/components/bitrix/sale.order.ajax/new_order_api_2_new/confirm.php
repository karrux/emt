<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Sale;

if(isset($arResult["ORDER"]["ID"]) && (int)$arResult["ORDER"]["ID"] > 0)
{
    $order = Sale\Order::load($arResult["ORDER"]["ID"]);
    $propertyCollection = $order->getPropertyCollection();
    $uEmail = $propertyCollection->getItemByOrderPropertyId(8)->getValue();
}

if (!empty($arResult["ORDER"])) {
    ?>
    <b style="font-size: 18px;"><?= GetMessage("SOA_TEMPL_ORDER_COMPLETE") ?></b>
    <?php
    CModule::IncludeModule('sale');
    $res = CSaleBasket::GetList(array(), array("ORDER_ID" => $arResult["ORDER"]["ID"])); // ID заказа
    $totalPrice = 0;
    $ITEMS[] = null;
    $arItemsID = array();
    while ($arItem = $res->Fetch()) {
        $ITEMS[] = $arItem;
        $arItemsID[] = $arItem['PRODUCT_ID'];
        $totalPrice = $totalPrice + $arItem['PRICE'] * $arItem['QUANTITY'];
    }

    if(count($arItemsID) > 0) {

        $arSelect = Array("ID", "NAME");
        $arFilter = Array("IBLOCK_ID"=>6, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arResult['BRANDS'][$arFields['ID']] = $arFields['NAME'];
        }

        $arSelect = Array("ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PROPERTY_*");
        $arFilter = Array("IBLOCK_ID"=>$arItemsID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID" => 8);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arFields['PROPERTIES'] = $ob->GetProperties();
            $arResult['PRODUCTS'][$arFields['ID']] = $arFields;
        }
    }


    $db_order = CSaleOrder::GetList(
        array("DATE_UPDATE" => "DESC"),
        array("ID" => $arResult["ORDER"]["ID"])
    );
    if ($arOrder = $db_order->Fetch())
    {
        $db_props = CSaleOrderProps::GetList(
            array("SORT" => "ASC")
        );
        if ($arProps = $db_props->Fetch())
        {
            $db_vals = CSaleOrderPropsValue::GetList(
                array(),
                array(
                    "ORDER_ID" => $arResult["ORDER"]["ID"],
                    "CODE" => array("NAME","EMAIL")
                )
            );
            while ($arVals = $db_vals->Fetch()){
                $Props[] = $arVals;
            }
        }
    }
    ?>
    <script>

        <?php
        $yandex_products = '';
        $i = 0;
        foreach ($ITEMS as $item):
        if(strlen(trim($item['NAME'])) == 0) Continue;

        $prod = $arResult['PRODUCTS'][$item['PRODUCT_ID']];

        if($GLOBALS['arTmpSections'][$prod['~IBLOCK_SECTION_ID']]) {
            $sec_name = $GLOBALS['arTmpSections'][$prod['~IBLOCK_SECTION_ID']];
        } else {
            $sec_name = '';
            $nav = CIBlockSection::GetNavChain(false, $prod['~IBLOCK_SECTION_ID']);
            while($s = $nav->Fetch()) {
                if(strlen($sec_name) > 0) $sec_name .= ' / ';
                $sec_name .= $s['NAME'];
            }
            $GLOBALS['arTmpSections'][$prod['~IBLOCK_SECTION_ID']] = $sec_name;
        }
        $i++;
        if($i > 1)
            $yandex_products.=', ';

        $yandex_products.='{
							"id": "'.$item['ID'].'",
							"name": "'.str_replace('"', "'", $item['NAME']).'",
							"price": '.$item['PRICE'].',
							"brand": "'.$arResult['BRANDS'][$prod['PROPERTIES']['BRAND']['VALUE']].'",
							"category": "'.str_replace('"', '\"', $sec_name).'",
							"quantity": '.$item['QUANTITY'].'
						}';

        ?>
        ga('ec:addProduct', {
            'id': '<?=$item['PRODUCT_ID'];?>',
            'name': '<?=str_replace("'", '"', $item['NAME']);?>',
            'price': '<?=$item['PRICE'];?>',
            'quantity': '<?=$item['QUANTITY'];?>',
            'category': '<?=str_replace("'", "\'", $sec_name)?>',
            'brand': '<?=$arResult['BRANDS'][$prod['PROPERTIES']['BRAND']['VALUE']]?>',
        });
        <?php endforeach;?>
        ga('ec:setAction', 'purchase', {
            'id': '<?=$arResult["ORDER"]["ID"]?>',
            'revenue': '<?=$totalPrice?>',
            'shipping': '<?=floatval($arResult['ORDER']['PRICE_DELIVERY'])?>',
            'affiliation': 'WhoGrill',
            'currency': 'RUB'  // local currency code.
        });
        ga('send', 'pageview');

        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            "ecommerce": {
                "purchase": {
                    "actionField": {
                        "id" : "<?=$arResult["ORDER"]["ID"]?>"
                    },
                    "products": [
                        <?=$yandex_products?>
                    ]
                }
            }
        });

        window.EMT = window.EMT || {};
        EMT.operation = {
            'task': 'sendOrder',
            'name': '<?=$Props[0]["VALUE"]?>',
            'email': '<?=$Props[1]["VALUE"]?>',
            'orderid': '<?=$arResult["ORDER"]["ID"]?>',
            'total': '<?=$arResult["ORDER"]["PRICE"]?>',
            'permission': 'subscribe' // unsubscribe
        };

    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2178176459127741');
        fbq('track', 'PageView');
        fbq('track', 'Purchase', {
            value: '<?=$totalPrice?>',
            currency:  '<?=$item['CURRENCY'];?>',
        });
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2178176459127741&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Тэг продажи Criteo OneTag -->
    <script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true" data-skip-moving="true"></script>
    <script type="text/javascript" data-skip-moving="true">
        window.criteo_q = window.criteo_q || [];
        var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
        window.criteo_q.push(
            { event: "setAccount", account: 61276}, // Это значение остается неизменным
            { event: "setEmail", email: "<?=($USER->IsAuthorized()) ? md5($USER->GetEmail()) : ''?>" }, // Может быть пустой строкой
            { event: "setSiteType", type: deviceType},
            { event: "trackTransaction", id: <?=$arResult["ORDER"]["ID"]?>, item: [
                    <?foreach ($ITEMS as $item):?>
                    <?if (!$item['PRODUCT_ID']) {continue;}?>
                    {id: "<?=$item['PRODUCT_ID'];?>", price: <?=$item['PRICE'];?>, quantity: <?=$item['QUANTITY'];?> },
                    <?endforeach?>
                ]}
        );
    </script>
    <!-- Конец тэга продажи Criteo OneTag -->
    <?
    if (!empty($arResult["PAY_SYSTEM"])) {
        ?>
        <br/><br/>

        <table class="sale_order_full_table" width="100%">
            <tr>
                <td class="ps_logo">
                    <div class="pay_name"><?= GetMessage("SOA_TEMPL_PAY") ?></div>
                    <?= CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false); ?>
                    <div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div>
                    <br>
                </td>
            </tr>

            <?
            if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0) {
                ?>
                <tr>
                    <td>
                        <?
                        if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y") {
                            ?>
                            <script language="JavaScript">
                                window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
                            </script>
                        <?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"])))) ?>
                            <?
                            if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE'])) {
                                ?><br/>
                                <?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"])) . "&pdf=1&DOWNLOAD=Y")) ?>
                                <?
                            }
                        } else {
                            if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]) > 0) {
                                try {
                                    $extraParameters['MNT_CUSTOM3'] = $uEmail;//MNT_CUSTOM3;
                                    include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                                } catch (\Bitrix\Main\SystemException $e) {
                                    if ($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
                                        $message = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
                                    else
                                        $message = $e->getMessage();

                                    echo '<span style="color:red;">' . $message . '</span>';
                                }
                            }
                        }
                        ?>
                    </td>
                </tr>
                <?
            }
            ?>
        </table>
        <?
    } ?>
    <br/><br/>
    <table class="sale_order_full_table">
        <tr>
            <td>
                <?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"])) ?>
                <br/><br/>
                <?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>
            </td>
        </tr>
    </table>

    <?
} else {
    ?>
    <b><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></b><br/><br/>

    <table class="sale_order_full_table">
        <tr>
            <td>
                <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"])) ?>
                <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
            </td>
        </tr>
    </table>
    <?
}
?>
<script>
    (function ($) {
        $(document).ready(function () {
            paySubmit = $('#order_form_div').find('form input[value="Оплатить"]');
            if (paySubmit.length > 0) {
                $('body').css('opacity', 0);
                paySubmit.closest('form').submit();
            }
        });
    })(jQuery);
</script>
