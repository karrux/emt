<?
$usLeftColumn = $APPLICATION->GetProperty("us_left_column");
if ($usLeftColumn == "Y")
    echo '</div><div class="clr"></div></div>';
?>
</div> <!-- /class="container" -->
</div> <!-- /id="main_block" -->

<div id="footer" class="bg_color_black">
    <div class="container">
        <div class="flexbox space_between">
            <div>
                © 2013&ndash;<?=date("Y");?><br/>Интернет-магазин грилей «WHOGRILL»<br>
                E-mail: <span onclick="yaCounter24186073.reachGoal('email_click'); GoogleEmailClick();"><a href="/contacts/">info@whogrill.ru</a></span><br>
                Телефон: <span class="call_phone_2">+7 (495) 125-21-49</span><br>
                Skype: <span class="call_phone_2">whogrill</span><br>
                <br>
                <noindex>
                    <a href="https://vk.com/whogrill" target="vk" rel="nofollow"><img src="/img/vk.png" width="24" height="24" alt="Вконтакте" align="left"/></a>
                    <a href="https://www.facebook.com/groups/whogril/" target="fb" rel="nofollow"><img src="/img/fb.png" width="24" height="24" alt="Facebook" hspace="5" align="left"/></a>
                    <a href="https://www.instagram.com/whogrill/" target="ig" rel="nofollow"><img src="/img/instagram-icon.png" width="24" height="24" alt="Instagram" hspace="5" align="left"/></a>
                </noindex>
                <div class="address_footer">
                    <ul>
                        <li><a href="https://yandex.ru/maps/-/CCWZNL6R" target="_blank"><i>Москва</i>, <br />Мичуринский проспект, 58к1</a></li>
                        <li><a href="https://yandex.ru/maps/-/CBqoUPRZDC" target="_blank"><i>Москва</i>, Калужское ш.,<br> ул. Сосновая 3Б</a></li>
                        <li><a href="https://yandex.ru/maps/-/CBBEqARtsD" target="_blank"><i>Санкт-Петербург</i>,<br> Межевой канал, д.5АХ</a></li>
                    </ul>
                </div>
            </div>
            <div>
                <b class="title_menu_in_footer">О компании</b>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", Array(
                    "ROOT_MENU_TYPE" => "about",	// Тип меню для первого уровня
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "about",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                ),
                    false
                );?>
            </div>
            <div>
                <b class="title_menu_in_footer">Информация</b>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", Array(
                    "ROOT_MENU_TYPE" => "info",	// Тип меню для первого уровня
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "info",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                ),
                    false
                );?>
            </div>
            <div>
                <b class="title_menu_in_footer">Каталог</b>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer_menu",
                    array(
                        "ROOT_MENU_TYPE" => "catalog",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "catalog",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "COMPONENT_TEMPLATE" => "footer_menu"
                    ),
                    false,
                    array(
                        "ACTIVE_COMPONENT" => "Y"
                    )
                );?>
            </div>
            <div>
                <b class="title_menu_in_footer">Города</b>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer_menu",
                    array(
                        "ROOT_MENU_TYPE" => "gril",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "gril",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "COMPONENT_TEMPLATE" => "footer_menu"
                    ),
                    false
                );?>
            </div>
            <div>
                <ul class="ul_oplata">
                    <li><img alt="Visa" src="/img/visa.png"></li>
                    <li><img alt="MasterCard" src="/img/master.png"></li>
                </ul>
                <br>
                <noindex><a rel="nofollow" class="yand_img" target="market"
                            href="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*https://market.yandex.ru/shop/222183/reviews">
                        <img src="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2505/*https://grade.market.yandex.ru/?id=222183&action=image&size=0" border="0" width="88" height="31"  alt=""/>
                    </a></noindex><br><br><br>
                <? if ($_SERVER['SCRIPT_NAME'] == "/index.php") { ?>
                    <img style="margin-right: 10px;" class="pull-left" src="/img/pixelplus.png" alt="Компания «Пиксель Плюс»" width="32" height="34"
                         title="Компания «Пиксель Плюс»">
                    <a href="https://www.pixelplus.ru/" target="pixel" class="no_style_link">Создание и продвижение сайта</a> —
                    <br>
                    компания «Пиксель Плюс»
                <? } else { ?>
                    <a style="margin-right: 10px;" class="pull-left" href="https://www.pixelplus.ru/" target="pixel"><img src="/img/pixelplus.png"
                                                                                                                          alt="Компания «Пиксель Плюс»" width="32"
                                                                                                                          height="34" title="Компания «Пиксель Плюс»"></a>
                    Создание и продвижение сайта —
                    <br>
                    компания «Пиксель Плюс»
                <? } ?>
            </div>
        </div>
    </div>
</div>

<div style="display:none;" itemscope itemtype="http://schema.org/LocalBusiness">
    <span itemprop="name">Магазин Whogrill</span>
    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <span itemprop="streetAddress">Калужское шоссе 4 км от мкад, поселение Сосенское, ул. Сосновая 3Б</span>
        <span itemprop="addressLocality">Москва</span>,
    </div>
    <span itemprop="telephone">+7 (495) 125-21-49</span>,
    <span itemprop="email">info@whogrill.ru</span>
    <img itemprop="image" src="/img/mos1_s.jpg">
    <span itemprop="priceRange">от 10 руб. до 849000 руб.</span>
    <time itemprop="openingHours" datetime="Mo-Su 11:00?20:00">ежедневно с 11-00 до 20-00</time>
</div>

<div style="display:none;" itemscope itemtype="http://schema.org/LocalBusiness">
    <span itemprop="name">Магазин Whogrill</span>
    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <span itemprop="streetAddress">Межевой канал, д.5, лит. АХ Балтийский Морской Центр левый шлагбаум открывается автоматически, 1 этаж налево</span>
        <span itemprop="addressLocality">Санкт-Петербург</span>,
    </div>
    <span itemprop="telephone">8 (812) 612-76-56</span>,
    <span itemprop="email">info@whogrill.ru</span>
    <img itemprop="image" src="/img/peter.jpg">
    <span itemprop="priceRange">от 10 руб. до 849000 руб.</span>
    <time itemprop="openingHours" datetime="Mo-Su 11:00?20:00">ежедневно с 11-00 до 20-00</time>
</div>

<!-- calltouch code -->
<script type="text/javascript">
    window.onRoistatModuleLoaded = function() {
        window.roistat.registerOnVisitProcessedCallback(function() {
            (function (w, d, nv, ls, yac){
                var lwait = function (w, on, trf, dly, ma, orf, osf) { var pfx = "ct_await_", sfx = "_completed";  if(!w[pfx + on + sfx]) { var ci = clearInterval, si = setInterval, st = setTimeout , cmld = function () { if (!w[pfx + on + sfx]) {  w[pfx + on + sfx] = true; if ((w[pfx + on] && (w[pfx + on].timer))) { ci(w[pfx + on].timer);  w[pfx + on] = null;   }  orf(w[on]);  } };if (!w[on] || !osf) { if (trf(w[on])) { cmld();  } else { if (!w[pfx + on]) { w[pfx + on] = {  timer: si(function () { if (trf(w[on]) || ma < ++w[pfx + on].attempt) { cmld(); } }, dly), attempt: 0 }; } } }   else { if (trf(w[on])) { cmld();  } else { osf(cmld); st(function () { lwait(w, on, trf, dly, ma, orf); }, 0); } }}};
                var ct = function (w, d, e, c, n){ var a = 'all', b = 'tou', src = b + 'c' + 'h';  src = 'm' + 'o' + 'd.c' + a + src; var jsHost = "https://" + src, s = [{"sp":"1","sc":d.createElement(e)}, {"sp":"2","sc":d.createElement(e)}, {"sp":"3","sc":d.createElement(e)}]; var jsf = function (w, d, s, h, c, n, yc) { if (yc !== null) { lwait(w, 'yaCounter'+yc, function(obj) { return (obj && obj.getClientID ? true : false); }, 50, 100, function(yaCounter) { s.forEach(function(el) { el.sc.async = 1; el.sc.src = jsHost + "." + "r" + "u/d_client.js?param;specific_id"+el.sp+";" + (yaCounter  && yaCounter.getClientID ? "ya_client_id" + yaCounter.getClientID() + ";" : "") + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":171110,\"roistat_visit\":"+window.roistat.visit+"}") + ";"; p = d.getElementsByTagName(e)[0]; p.parentNode.insertBefore(el.sc, p); }); }, function (f) { if(w.jQuery) { w.jQuery(d).on('yacounter' + yc + 'inited', f ); }}); } else { s.forEach(function(el) { el.sc.async = 1; el.sc.src = jsHost + "." + "r" + "u/d_client.js?param;specific_id"+el.sp+";" + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":171110,\"roistat_visit\":"+window.roistat.visit+"}") + ";"; p = d.getElementsByTagName(e)[0]; p.parentNode.insertBefore(el.sc, p); }); } };if (!w.jQuery) { var jq = d.createElement(e); jq.src = jsHost + "." + "r" + 'u/js/jquery-1.7.min.js'; jq.onload = function () { lwait(w, 'jQuery', function(obj) { return (obj ? true : false); }, 30, 100, function () { jsf(w, d, s, jsHost, c, n, yac);  }); }; p = d.getElementsByTagName(e)[0]; p.parentNode.insertBefore(jq, p); } else { jsf(w, d, s, jsHost, c, n, yac); } };
                var gaid = function (w, d, o, ct, n) { if (!!o) { lwait(w, o, function (obj) {  return (obj && obj.getAll ? true : false); }, 200, (nv.userAgent.match(/Opera|OPR\//) ? 10 : 20), function (gaCounter) { var clId = null; try {  var cnt = gaCounter && gaCounter.getAll ? gaCounter.getAll() : null; clId = cnt && cnt.length > 0 && !!cnt[0] && cnt[0].get ? cnt[0].get('clientId') : null; } catch (e) { console.warn("Unable to get clientId, Error: " + e.message); } ct(w, d, 'script', clId, n); }, function (f) { w[o](function () {  f(w[o]); })});} else{ ct(w, d, 'script', null, n); }};
                var cid  = function () { try { var m1 = d.cookie.match('(?:^|;)\\s*_ga=([^;]*)');if (!(m1 && m1.length > 1)) return null; var m2 = decodeURIComponent(m1[1]).match(/(\d+\.\d+)$/); if (!(m2 && m2.length > 1)) return null; return m2[1]} catch (err) {}}();
                if (cid === null && !!w.GoogleAnalyticsObject){
                    if (w.GoogleAnalyticsObject=='ga_ckpr') w.ct_ga='ga'; else w.ct_ga = w.GoogleAnalyticsObject;
                    if (typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1){new Promise(function (resolve) {var db, on = function () {  resolve(true)  }, off = function () {  resolve(false)}, tryls = function tryls() { try { ls && ls.length ? off() : (ls.x = 1, ls.removeItem("x"), off());} catch (e) { nv.cookieEnabled ? on() : off(); }};w.webkitRequestFileSystem ? webkitRequestFileSystem(0, 0, off, on) : "MozAppearance" in d.documentElement.style ? (db = indexedDB.open("test"), db.onerror = on, db.onsuccess = off) : /constructor/i.test(w.HTMLElement) ? tryls() : !w.indexedDB && (w.PointerEvent || w.MSPointerEvent) ? on() : off();}).then(function (pm){
                        if (pm){gaid(w, d, w.ct_ga, ct, 2);}else{gaid(w, d, w.ct_ga, ct, 3);}})}else{gaid(w, d, w.ct_ga, ct, 4);}
                }else{ct(w, d, 'script', cid, 1);}})
            (window, document, navigator, localStorage, "24186073");
        });
    }
</script>
<!-- /calltouch code -->
<?/*
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter24186073 = new Ya.Metrika2({
                    id:24186073,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/24186073" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
*/?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(24186073, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        trackHash:true,
        ecommerce:"dataLayer"
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/24186073" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<?if ($APPLICATION->GetCurPage(false) != '/personal/basket/') :?>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2178176459127741');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2178176459127741&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->
<?endif;?>

<script>
    jQuery(document).on("click", 'form[name="ORDER_FORM"] input[name="submitbutton"]', function() {
        var m =$(this).closest('form');
        var fio = m.find('input[name="ORDER_PROP_1"]').val();
        var fam = m.find('input[name="ORDER_PROP_2"]').val();
        var phone = m.find('input[name="ORDER_PROP_3"]').val();
        var mail = m.find('input[name="ORDER_PROP_8"]').val();
        var sub = 'Заказ';
        if (phone!='' && phone!=undefined && fio!='' && mail!='' && fam!=''){
            $.getJSON('http://api.calltouch.ru/calls-service/RestAPI/requests/orders/register/', {
                clientApiId: '840888358ct27764075aec8243e538b3106697c609c',
                subject: sub,
                fio: fio+' '+fam,
                email: mail,
                phoneNumber: phone,
                sessionId: window.call_value
            });
        }
    });
</script>


<script>
    <?$APPLICATION->ShowViewContent('GOOGLE_ANALYTICS_FOOTER');?>
</script>

<!-- ROISTAT BEGIN -->
<script>
    (function(w, d, s, h, id) {
        w.roistatProjectId = id; w.roistatHost = h;
        var p = d.location.protocol == "https:" ? "https://" : "http://";
        var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
        var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
    })(window, document, 'script', 'cloud.roistat.com', '3e0719770961d989551b422e72ac6465');
</script>
<script>
    function ReplacePhone() {
        this.currentAttempts = 0;
        this.maxAttempts = 200; // не верю что на странице может быть больше 200 номеров
        this.phones = [];

        this.clearPhone = function (phone) {
            return phone.replace(/\D+/g,"");
        };

        this.setPhones = function (phones) {
            var self = this;

            phones.forEach(function(item, key) {
                var cleanPhones = [];

                var cleanPhone = self.clearPhone(item.phone);
                if (cleanPhone.length > 10) {
                    var minPhone = cleanPhone.substr(1);

                    self.phones.push({'phone': "8" + minPhone, 'class': item.class});
                    self.phones.push({'phone': "7" + minPhone, 'class': item.class});
                    self.phones.push({'phone':       minPhone, 'class': item.class});
                } else {
                    self.phones.push({'phone':       cleanPhone, 'class': item.class});
                }
            });
        };

        this.searchPhone = function (phone) {
            for (var i = 0; i < this.phones.length; i++) {
                if (this.phones[i]['phone'] === phone) {
                    return this.phones[i];
                }
            }
            return false;
        };

        this.start = function () {
            var attempts = 1;
            for (var i = 0; i < attempts; i++) {
                this.foreachChildren('body');
            }
            this.setClassForPhonesOnHref();
        };

        this.foreachChildren = function (elem) {
            var children;
            if(typeof elem == 'string'){
                children = document.querySelector(elem).children;
            }else{
                children = elem.children;
            }

            if (children.length > 0) {
                for (var i = 0; i < children.length; i++) {
                    if (children[i].tagName != "SCRIPT") {
                        this.foreachChildren(children[i]);
                    }
                }
            }

            if(elem.tagName !== "BODY") {
                this.findPhonesOnElement(elem);
            }
        };

        this.findPhonesOnElement = function (elem = 'body') {
            var myArray;
            var html = elem.outerHTML;
            var regex = /(roistat-phone[^>]*?)?>[^<>]*?((\+?\d+[\s\-\.]?)?((\(\d+\)|\d+)[\s\-\.]?)?(\d[\s\-\.]?){6,7}\d)[^<>]*?</gm;
            while ((myArray = regex.exec(html)) != null) {
                this.replaceContent(elem, myArray);
            }
        };

        this.replaceContent = function (elem, myArray) {
            var hasRoistatClass = myArray[1];
            var number = myArray[2];
            var clearNumber = number.replace(/\D+/g,"");
            var search = this.searchPhone(clearNumber);
            var hasClass;
            if (elem.classList) {
                hasClass = elem.classList.contains(search['class']);
            } else {
                hasClass = new RegExp('(^| )' + search['class'] + '( |$)', 'gi').test(elem.className);
            }
            if (search && !hasClass && !hasRoistatClass && !elem.innerHTML.match('<script>')) {
                if (this.currentAttempts++ > this.maxAttempts) {
                    return false; // Скорее всего мы вошли в вечный цикл
                }
                if (elem.innerHTML.trim() == number) {
                    if (elem.classList) {
                        elem.classList.add(search['class']);
                    } else {
                        elem.className += ' ' + search['class'];
                    }
                } else {
                    elem.innerHTML = elem.innerHTML.replace(number, '<span class="'+search['class']+'">'+number+'</span>');
                    this.findPhonesOnElement(elem); // Еще раз проверяем элемент на наличие других номеров
                }
            }
        };

        this.setClassForPhonesOnHref = function () {
            var self = this;
            var elements = document.querySelectorAll('[href^="tel:"]');
            elements.forEach(function(elem, key) {
                var phone = self.clearPhone(elem.getAttribute('href'));
                var search = self.searchPhone(phone);
                if (search && !elem.classList.contains(search['class'])) {
                    elem.classList.add(search['class']+'-tel');
                }
            });
        };
    }

    var phoneReplacer = new ReplacePhone();
    phoneReplacer.setPhones([
        {'phone': '+7 (495) 125-21-49', 'class': 'roistat-phone495'},
        {'phone': '+7 (812) 612-76-56', 'class': 'roistat-phone812'},
        {'phone': '+7 (800) 775-45-26', 'class': 'roistat-phone800'},
    ]);
    phoneReplacer.start();
</script>

<!-- BEGIN JIVOSITE INTEGRATION WITH ROISTAT -->
<script type='text/javascript'>
    var getCookie = window.getCookie = function (name) {
        var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };
    function jivo_onLoadCallback() {
        jivo_api.setUserToken(JSON.stringify({roistat_visit: getCookie('roistat_visit'), roistat_marker: getCookie('roistat_marker')}));
    }
</script>
<!-- END JIVOSITE INTEGRATION WITH ROISTAT -->

<!-- ROISTAT END -->
  <script src="https://emailtools.ru/js/api/v1/tools.js" defer></script>
  <!--<script src="/js/emt_integration.js"></script>-->
</body>
</html>
