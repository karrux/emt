<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="ru">
    <head>
        <title><? $APPLICATION->ShowTitle() ?></title>

        <?if($_REQUEST["set_filter"]||$_REQUEST["filter"]||$_REQUEST["action"]):
            $APPLICATION->SetPageProperty("robots", "noindex, follow");
            ?>
        <?else:?>
            <?$APPLICATION->SetPageProperty("robots", "all");?>
        <?endif;?>
        <?if ($APPLICATION->GetCurPage(true) == SITE_DIR."index.php"){?>
            <meta name="google-site-verification" content="Orvo8jN-kxbT2RUXuhxswB47Lz7XFxbJ3epI65lTCk0" />
        <?}?>
        <meta name="viewport" content="width=device-width" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

        <?
        CJSCore::Init(array("jquery"));
        $APPLICATION->ShowHead();
        Asset::getInstance()->addJs('/js/emt_tool.js');
        Asset::getInstance()->addJs('/js/jivosite.code.js');
        Asset::getInstance()->addJs('/js/project.js');

        Asset::getInstance()->addCss('/scripts/fancybox/jquery.fancybox.css');
        Asset::getInstance()->addJs('/scripts/fancybox/jquery.fancybox.pack.js');

        Asset::getInstance()->addCss('/js/jquery.nouislider.css');
        Asset::getInstance()->addJs('/js/jquery.nouislider.min.js');

        Asset::getInstance()->addCss('/js/core-ui-select.css');
        Asset::getInstance()->addJs('/js/jquery.core-ui-select.js');

        Asset::getInstance()->addCss('/js/flexslider/flexslider.css');
        Asset::getInstance()->addJs('/js/flexslider/jquery.flexslider-min.js');

        Asset::getInstance()->addJs('/js/jquery.liblink.js');
        Asset::getInstance()->addJs('/js/jquery.cookie.js');
        Asset::getInstance()->addJs('https://cdn.jsdelivr.net/stopsovetnik/latest/ss.min.js');
        Asset::getInstance()->addJs('/js/jquery.maskedinput.js');

        Asset::getInstance()->addCss('/bitrix/templates/new_whogrill/basket_fix.css');

        Asset::getInstance()->addCss('/js/tooltip/tooltipster.bundle.min.css');
        Asset::getInstance()->addJs('/js/tooltip/tooltipster.bundle.min.js');

        if (strpos($APPLICATION->GetCurDir(), "/gril/") !== false) {
            Asset::getInstance()->addJs('/scripts/window.history.js');
        }

        Asset::getInstance()->addString('<script type="text/javascript">var hunter_code="c5d0e24eb8c2dc4e1d08e8093ed9a8d2";</script>');
        ?>

        <!-- GA -->

        <script data-skip-moving="true">
            product_position = 0;

            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-64695489-1', 'auto');
            ga('require', 'ec');

            <?$APPLICATION->ShowViewContent('GOOGLE_ANALYTICS');?>

            ga('send', 'pageview');

        </script>
        <!-- GA -->

    </head>
<body>


<? $APPLICATION->ShowPanel(); ?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
    "AREA_FILE_SHOW" => "file",
    "PATH" => "/include/fixed_header_style2.php",
    "EDIT_TEMPLATE" => ""
),
    false,
    array('HIDE_ICONS' => 'Y')
);?>

    <div id="header">

        <div class="bg_color_red">
            <div class="container">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header_menu",
                    Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "header",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(0=>"",),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "header",
                        "USE_EXT" => "N"
                    )
                );?>
            </div>
        </div>
        <div class="container">
            <div class="flexbox space_between header-main" style="padding: 1.5em 0;" >
                <div>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/logo.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                        false
                    );?>
                </div>
                <div>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/logo2.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                        false
                    );?>
                </div>
                <div align="center">
				<span class="call_phone_1_1">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/phone_1.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                        false
                    );?>
                    <br>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/phone_1_2.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                        false
                    );?>
                    <br>
				</span>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/phone_2.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                        false
                    );?>
                    <br>
                    <span class="font_size_11">бесплатный номер для регионов</span>
                </div>
                <div align="center">
                    <div class="header_address">
                        <a href="https://yandex.ru/maps/-/CCWZNL6R" target="_blank"><i>Москва</i>, <span>Мичуринский проспект, 58к1</span></a><br>
                        <a href="https://yandex.ru/maps/-/CBqoUPRZDC" target="_blank"><i>Москва</i>, <span>Калужское ш., ул. Сосновая 3Б</span></a><br>
                        <a href="https://yandex.ru/maps/-/CBBEqARtsD" target="_blank"><i>Санкт-Петербург</i>, <span>Межевой канал, д.5АХ</span></a>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:search.title",
                        "header_search",
                        Array(
                            "CATEGORY_0" => array(0=>"iblock_content",),
                            "CATEGORY_0_TITLE" => "",
                            "CATEGORY_0_iblock_content" => array(0=>"8",),
                            "CHECK_DATES" => "N",
                            "COMPONENT_TEMPLATE" => "header_search",
                            "CONTAINER_ID" => "search-form",
                            "CONVERT_CURRENCY" => "N",
                            "INPUT_ID" => "title-search-input",
                            "NUM_CATEGORIES" => "1",
                            "ORDER" => "rank",
                            "PAGE" => "#SITE_DIR#search/index.php",
                            "PREVIEW_HEIGHT" => "75",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PREVIEW_WIDTH" => "75",
                            "PRICE_CODE" => array(0=>"BASE",),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "SHOW_INPUT" => "Y",
                            "SHOW_OTHERS" => "N",
                            "SHOW_PREVIEW" => "Y",
                            "TOP_COUNT" => "5",
                            "USE_LANGUAGE_GUESS" => "Y"
                        )
                    );?>
                    <?
                    // $APPLICATION->IncludeComponent("bitrix:search.form", "header_search", Array(
                    // "PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
                    // "USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
                    // ),
                    // false
                    // );
                    ?>
                    <div class="working-hours">
                        <div class="working-hours__shop" >11:00-20:00</div><div class="working-hours__phone">09:00-22:00</div>
                    </div>
                </div>
                <div align="center">
                    <?if(!($USER->IsAuthorized())) { ?>
                        <a class="font_size_11" href="/personal/history/">Авторизация</a>
                    <? } else { ?>
                        <a class="font_size_11" href="/personal/history/">Личный&nbsp;кабинет</a>
                        /
                        <a class="font_size_11" href="?logout=yes">Выйти</a>
                    <? } ?>
                    <div class="header_basket_block">
                        <div class="basket_to_replace">
                            <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "basket_top", Array(
                                "PATH_TO_BASKET" => "/personal/basket.php", // Страница корзины
                                "PATH_TO_ORDER" => "/personal/order.php", // Страница оформления заказа
                                "SHOW_DELAY" => "Y",
                                "SHOW_NOTAVAIL" => "Y",
                                "SHOW_SUBSCRIBE" => "Y"
                            ),
                                false
                            );?>
                        </div>
                        <div class="alink" data-url="/personal/basket/"><img alt="Корзина" src="/img/basket.png" height="27"/><br><span class="basket_link font_size_11">Корзина</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-image"></div>
        <div class="bg_color_red">
            <div class="container padding_0">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top_menu_new",
                    array(
                        "ROOT_MENU_TYPE" => "top",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "2",
                        // "MAX_LEVEL" => ($USER->IsAdmin() ? 2 : 1),
                        "CHILD_MENU_TYPE" => "top",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "top_menu_new"
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
<div id="main_block">
    <div class="container">
<?
$usLeftColumn = $APPLICATION->GetProperty("us_left_column");
if ($usLeftColumn == "Y")
    echo '<div><div class="pull-left column-content-left">';
?>
<?
/*
 * Использование левой колонки — Инструкция
 *
 * Если необходимо использовать левую колонку, положи в нужный раздел файл "sect_inc.php"
 * Для этого раздела установи свойство раздела: Вывод левой колонки [us_left_column] = "Y"
 */
?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
    "AREA_FILE_SHOW" => "sect",
    "AREA_FILE_SUFFIX" => "inc",
    "AREA_FILE_RECURSIVE" => "Y",
    "EDIT_TEMPLATE" => ""
),
    false
);?>
<?$APPLICATION->ShowViewContent('new_smartfilter');?>
<?if ($usLeftColumn == "Y")
    echo '</div><div class="pull-left column-content-right">';
?>
<? if ($_SERVER['SCRIPT_NAME'] != "/index.php") { ?>
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "nav", Array(
        "START_FROM" => "0", // Номер пункта, начиная с которого будет построена навигационная цепочка
        "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
        "SITE_ID" => "-", // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
    ),
        false
    );?>
<? } ?>