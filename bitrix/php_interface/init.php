<?
define("MNT_CUSTOM3", 'info@whogrill.ru');
require_once dirname(__FILE__)."/include/pixelplus/dump.php";
require_once dirname(__FILE__)."/include/pixelplus/mailevent.php";
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");

require_once dirname(__FILE__)."/include/pixelplus/check_mobile/check_mobile.php";
use Bitrix\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;


CModule::AddAutoloadClasses(
    "",
    array(
        "CPixelPlusTools"=>"/bitrix/php_interface/include/pixelplus/classes/pixelplustools.php",
        "CProjectHandlers"=>"/bitrix/php_interface/include/pixelplus/classes/projecthandlers.php",
    )
);

//AddEventHandler("main", "OnEndBufferContent", Array("CProjectHandlers", "deleteKernelJs"));
//AddEventHandler("main", "OnEndBufferContent", Array("CProjectHandlers", "deleteKernelCss"));

AddEventHandler("main", "OnBeforeLocalRedirect", Array("CProjectHandlers", "OnBeforeLocalRedirect"));
//AddEventHandler("main", "OnEpilog", Array("CProjectHandlers", "OnEpilog"));
AddEventHandler("main", "OnEpilog", Array("CProjectHandlers", "addMeta"));

AddEventHandler("sale", "OnOrderNewSendEmail", Array("CProjectHandlers", "OnOrderNewSendEmail"));
AddEventHandler("sale", "OnBeforeBasketAdd", Array("CProjectHandlers", "OnBeforeBasketAdd"));
//AddEventHandler("sale", "OnSaleCalculateOrder", Array("CProjectHandlers", "OnSaleCalculateOrder"));

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler("main", "OnBeforeEventSend",  array("mailHandler", "mail_send"));
$eventManager->addEventHandler("main", "OnBeforeUserAdd", array("mailHandler", "before_add_user"));
$eventManager->addEventHandler("main", "OnEndBufferContent", array("mailHandler", "end_buffer_content"));

/* Замена "%2F" на "/" в ссылках перед выводом на сайт */
AddEventHandler("main", "OnEndBufferContent", "ChangeMyContent");
function ChangeMyContent(&$content)
{
    $content = str_replace("%2F", "/", $content);
}
/* END Замена "%2F" на "/" в ссылках перед выводом на сайт */

//Отправка в гугл аналитику
AddEventHandler("main", "OnSaleComponentOrderOneStepFinal", Array("OnSaleComponentOrderOneStepFinal"));
function OnSaleComponentOrderOneStepFinal(&$arOrder){
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/proverka.txt',"\n" . var_export($arOrder, true));
}

if(strpos($_SERVER["HTTP_HOST"], 'm.whogrill.ru')!==false){
    \Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'OnSaleComponentOrderOneStepFinal',
        array('\Whogrill\Handlers\Sale\OrderAjaxComponentHandler', 'AddGoogleAnalyticsEcommerceCode')
    );

    \Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
        '\Whogrill\Handlers\Sale\OrderAjaxComponentHandler' => '/bitrix/php_interface/classes/handlers/OrderAjaxComponentHandler.php',
    ));
}
// yandex commerce
\Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'OnSaleComponentOrderOneStepFinal',
    array('\Whogrill\Handlers\Sale\OrderAjaxComponent', 'AddYandexMetricsEcommerceCode')
);
\Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
    '\Whogrill\Handlers\Sale\OrderAjaxComponent' => '/bitrix/php_interface/classes/handlers/OrderAjaxComponentHandler.php',
));


//add basket	

/*AddEventHandler("sale", "OnBeforeBasketAdd","AddCommerceBasket");
function AddCommerceBasket(&$arFields){
global $APPLICATION;	
    AddMessage2Log("add basket");
	AddMessage2Log($arFields);
	
	 $output_add = [
            'ecommerce' => [
                'add' => [
                    'products' => [
                        'id' => $arFields["PRODUCT_ID"],
						'name' => $arFields["NAME"],
						'price' => $arFields["PRICE"],
						'quantity' => (int)$arFields['QUANTITY']
                    ]
                ]
            ]
        ];
		
	\Bitrix\Main\Page\Asset::getInstance()->addString(
    '<script>console.log("TTTTTTTTTTTT");</script>',
    true
);

	echo '<script>(window.dataLayer || []).push(' . \CUtil::PhpToJSObject($output_add, false, true, true) . ');</script>';
	
	\Bitrix\Main\Page\Asset::getInstance()->addString(
    '<script>(window.dataLayer || []).push(' . \CUtil::PhpToJSObject($output_add, false, true, true) . ');</script>',
    true
);	
	
	AddMessage2Log(\CUtil::PhpToJSObject($output_add, false, true, true));
}
*/


/**
 * Устанавливает канонический адрес для всех страниц с неизвестными параметрами
 */
if (count($_GET) > 0) {
    AddEventHandler("main", "OnPageStart", "setDefaultCanonical");

    function setDefaultCanonical() {
        global $APPLICATION;

        $canonical = ($_SERVER['HTTPS'] || $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
        $canonical .= (SITE_SERVER_NAME) ? SITE_SERVER_NAME : $_SERVER["SERVER_NAME"];
        $canonical .= $APPLICATION->GetCurPage(false);
        if (IntVal($_GET['PAGEN_1']) > 1) {
            $canonical .=  '?PAGEN_1=' . IntVal($_GET['PAGEN_1']);
        }
        $APPLICATION->SetPageProperty('canonical', $canonical);
    }
}

/**
 * Добавляет № страницы к мета параметрам страницы
 */
if ($_REQUEST['PAGEN_1'] >= 1) {
    AddEventHandler("main", "OnEpilog", "metaModify");

    function metaModify () {
        global $APPLICATION;
        $addString = ' - страница '.IntVal($_REQUEST['PAGEN_1']);
        if (($pageTitle = $APPLICATION->GetTitle()) && strpos($pageTitle, 'траница') === false){
            $APPLICATION->SetTitle($pageTitle.$addString);
        }
        if (($title = $APPLICATION->GetTitle('title')) && strpos($title, 'траница') === false) {
            $APPLICATION->SetPageProperty('title', $title.$addString);
        }
        if (($description = $APPLICATION->GetProperty('description')) && strpos($description, 'траница') === false) {
            $APPLICATION->SetPageProperty('description', $description.$addString);
        }
        if (($keywords = $APPLICATION->GetProperty('keywords')) && strpos($keywords, 'траница') === false) {
            $APPLICATION->SetPageProperty('keywords', $keywords.$addString);
        }
    }
}

//Редирект с неполноценных ссылок
AddEventHandler("main", "OnEpilog", "SmallUrlRedirect");
function SmallUrlRedirect()
{
    CModule::IncludeModule('iblock');
    if(isset($_REQUEST['ELEMENT_CODE']) && isset($_REQUEST['SECTION_CODE'])){

        $arFilter = Array("IBLOCK_ID"=> 8, "ACTIVE"=>"Y", 'CODE'=> $_REQUEST['ELEMENT_CODE']);
        $res = CIBlockElement::GetList(Array(), $arFilter);
        if($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $sectionID = $arFields['IBLOCK_SECTION_ID'];
            $arFiltar = array('ID' => $sectionID); // Фильтруем по id раздела
            $rsSect = CIBlockSection::GetList(array(),$arFiltar);
            $arSect = $rsSect->GetNext();
            $newURL = $arSect["LIST_PAGE_URL"].$arSect["CODE"]; // Склеиваем начало URL
            $lastURL = $_REQUEST['ELEMENT_CODE']; // Получаем конец URL
            $canonicalURL = $newURL."/".$lastURL.".html";// Склеиваем конец URL с началом
            $haystack = $_SERVER['REDIRECT_URL'];
            $needle = $canonicalURL;
            $pos = strripos($haystack, $needle); // Ищем совпадение, если совпадения нету значит url не полный и на нем выводится canonical полный ссылки на товар
            if ($pos === false) {
                header("Location:".$canonicalURL,true,301);
                exit;
                //$APPLICATION->AddHeadString('<link href="https://whogrill.ru' . $canonicalURL . '" rel="canonical" />', true); // Выводим canonical
            }
        }
    }
}





/*Выозв 404 страницы*/
define("PREFIX_PATH_404", "/404.php");

AddEventHandler("main", "OnAfterEpilog", "Prefix_FunctionName");

function Prefix_FunctionName() {
    global $APPLICATION;

    // Check if we need to show the content of the 404 page
    if (!defined('ERROR_404') || ERROR_404 != 'Y') {
        return;
    }

    // Display the 404 page unless it is already being displayed
    if ($APPLICATION->GetCurPage() != PREFIX_PATH_404) {
        \CHTTP::setStatus("404 Not Found");
        header('X-Accel-Redirect: '.PREFIX_PATH_404);
        exit();
    }
}

AddEventHandler("search", "BeforeIndex", Array("IncludeInfo", "BeforeIndexHandler"));

class IncludeInfo
{
    // создаем обработчик события "BeforeIndex"
    function BeforeIndexHandler($arFields)
    {
        if ($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 8 && $arFields['ITEM_ID'] > 0) { // ID Инфоблока
            $arSelect = Array("ID", "PROPERTY_53");
            $arFilter = Array("IBLOCK_ID" => 8, "ID" => $arFields['ITEM_ID']);
            $rsItems = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            if ($arItem = $rsItems->GetNext()) {
                if($arFields["TITLE"]){
                    $arFields["TITLE"] .= ' ' . $arItem['PROPERTY_53_VALUE'];
                }
            }
            if ($arFields["BODY"]) { // Поиск только по заголовку
                $arFields["BODY"] = ''; //.= ' ' . $arItem['PROPERTY_53_VALUE'];
            }
        }
        return $arFields;
    }
}

AddEventHandler("sale", "OnSaleComponentOrderResultPrepared", "OrderPre");
function OrderPre($order, &$arUserResult, $request, &$arParams, &$arResult) {
    $op = json_decode(Application::getInstance()->getContext()->getRequest()->getCookie("ORDER_PROPERTIES"));
    $order_props = $op;//iconv("UTF-8", "windows-1251", $op);
    $location = "";
    $person = array();
    $arResult["REFRESH"] = '';

    if(!empty($order_props)) {

        foreach ($arResult['JS_DATA']['DELIVERY'] as $key => &$deliv) {
            if ($order_props->DELIVERY_ID == $deliv['ID']) $deliv['CHECKED'] = 'Y';
            else unset($deliv['CHECKED']);
        }

        foreach ($arResult['JS_DATA']['PAY_SYSTEM'] as $key => &$pay) {
            if ($order_props->PAY_SYSTEM_ID == $pay['ID']) $pay['CHECKED'] = 'Y';
            else unset($pay['CHECKED']);
        }

        /*foreach ($arResult['JS_DATA']['PERSON_TYPE'] as $key => &$person) {
            if ($order_props->PERSON_TYPE == $person['ID']){
                $person['CHECKED'] = 'Y';
                $person['SORT'] = 50;
            }
            else unset($person['CHECKED']);
        }*/

        $arResult["JS_PERSON_TYPE"] = $order_props->PERSON_TYPE;


        foreach ($arResult['ORDER_PROP']['USER_PROPS_Y'] as $key => &$property) {
            $ordprop = 'ORDER_PROP_'.$property['ID'];
            if ($order_props->$ordprop) {
                $property['VALUE'] = $order_props->$ordprop;					//iconv("UTF-8", "windows-1251",$order_props->$ordprop);
                $property['~VALUE'] = $order_props->$ordprop;					//iconv("UTF-8", "windows-1251",$order_props->$ordprop);
            }
            if ($property['ID'] == 7 ) {
                $location = $order_props->$ordprop;

                foreach ($property['VARIANTS'] as $vkey => $variant) {
                    if($variant['ID'] == $location) {
                        $arResult['ORDER_PROP']['USER_PROPS_Y'][$key]['VARIANTS'][$vkey]['SELECTED'] = 'Y';
                    } else {
                        $arResult['ORDER_PROP']['USER_PROPS_Y'][$key]['VARIANTS'][$vkey]['SELECTED'] = '';
                    }
                }

                $location_prop_id = $property['ID'];
                //$arResult['LOCATIONS'] = getLocationsResult($location, $location_prop_id);
            }
        }


        $psystem = 'PAY_SYSTEM_ID';
        if($psystem) {
            foreach ($arResult['PAY_SYSTEM'] as $pkey => $pay) {
                if($pay['ID'] == $order_props->$psystem) {
                    $arResult['PAY_SYSTEM'][$pkey]['CHECKED'] = 'Y';
                    $arResult['ORDER_DATA']['PAY_SYSTEM_ID'] = $order_props->$psystem;
                    $arResult['USER_VALS']['PAY_SYSTEM_ID'] = $order_props->$psystem;
                } else {
                    $arResult['PAY_SYSTEM'][$pkey]['CHECKED'] = '';
                }
            }
        }



        $deliveryId = 'DELIVERY_ID';
        if($deliveryId) {
            foreach ($arResult['DELIVERY'] as $dkey => $delivery) {
                if($delivery['ID'] == $order_props->$deliveryId) {
                    $arResult['DELIVERY'][$dkey]['CHECKED'] = 'Y';
                    $arResult['ORDER_DATA']['DELIVERY_ID'] = $order_props->$deliveryId;
                    $arResult['USER_VALS']['DELIVERY_ID'] = $order_props->$deliveryId;
                } else {
                    $arResult['DELIVERY'][$dkey]['CHECKED'] = '';
                }
            }
        }





        $ordpropD = 'ORDER_DESCRIPTION';
        $arResult['USER_VALS']['~ORDER_DESCRIPTION'] = $order_props->$ordpropD;//iconv("UTF-8", "windows-1251",$order_props->$ordpropD);
        $arResult['USER_VALS']['ORDER_DESCRIPTION'] = $order_props->$ordpropD;//iconv("UTF-8", "windows-1251",$order_props->$ordpropD);
        $arResult["REFRESH"] = 'Y';
        global $APPLICATION;
        $APPLICATION->set_cookie("ORDER_PROPERTIES", '', false, "/", "whogrill.ru");
        //print "<pre>";
        //print_r($arResult);
        //print "</pre>";
    }
    else
    {
        /*

        foreach ($arResult['ORDER_PROP']['USER_PROPS_Y'] as $key => &$property)
        {


            if ($property['ID'] == 7 ) {

                foreach ($property['VARIANTS'] as $vkey => $variant)
                {
                    $arResult['ORDER_PROP']['USER_PROPS_Y'][$key]['VARIANTS'][$vkey]['SELECTED'] = '';
                }
            }
        }
        */

    }

}

// ROISTAT BEGIN

Bitrix\Main\Loader::registerAutoLoadClasses(null, [
    'Roistat\\Roistat' => '/local/classes/Roistat.php',
    'Roistat\\RoistatEventHook' => '/local/classes/RoistatEventHook.php',
]);

$eventManager = \Bitrix\Main\EventManager::getInstance();
// Создание заказа
$eventManager->addEventHandler('sale', 'OnSaleOrderSaved', array(
    "Roistat\\RoistatEventHook",
    "rsOnAddOrder"
));
// Отправка формы
$eventManager->addEventHandler('form', 'onAfterResultAdd', array(
    "Roistat\\RoistatEventHook",
    "rsOnFormAdd"
));
// ROISTAT END

\Bitrix\Main\EventManager::getInstance()->addEventHandler("main", "OnEndBufferContent", "removeSpacesAndTabs");

/**
 * Удаляет табы и лишние пробелы из html кода
 *
 * @param $content
 */
function removeSpacesAndTabs (&$content) {
    global $APPLICATION;
    $page = $APPLICATION->GetCurPage();

    if($page != '/bitrix/tools/captcha.php' && $page != '/bitrix/admin/captcha.php')
    {
        $content = str_replace(array('	', '    '), ' ', $content);
        $content = str_replace(array("\n\n"), "\n", $content);
    }

}

if (!function_exists('printr')) {
    function printr($array) {
        GLOBAL $USER;
        if (!$USER->IsAdmin()) return false;
        $args = func_get_args();
        if (count($args) > 1) {
            foreach ($args as $values)
                printr($values);
        } else {
            if (is_array($array) || is_object($array)) {
                printf("<pre>");
                print_r($array);
                printf("</pre>");
            } else {
                echo $array;
            }
        }
        return true;
    }
}


/* EMT Tool Form Submit Check*/
function onAfterResultAddUpdate($WEB_FORM_ID, $RESULT_ID)
{
    // действие обработчика распространяется только на форму с ID=1
    if ($WEB_FORM_ID == 1)
    {
        $res = CFormResult::GetDataByID($RESULT_ID,array(),$arResult,$arAnswer);
    	?>
			<script>
                window.EMT = window.EMT || {};
                EMT.operationSend({
                    'task': 'sendEmail',
                    'name': '<?=$res["SIMPLE_QUESTION_283"][0]["USER_TEXT"]?>',
                    'email': '<?=$res["SIMPLE_QUESTION_548"][0]["USER_TEXT"]?>'
                });
			</script>
		<?
    }
}

AddEventHandler('form', 'onAfterResultAdd', 'onAfterResultAddUpdate');
?>
